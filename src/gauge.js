(function() {
    window.loadDB2Gauge = function(self, data, extraConfigs) {
        var config = { // master config with defaults
            colorDetractor: '#a8000b',
            colorNeutral: '#ffbb19',
            colorPromoter: '#00847e',
            showCount: false,
            decimalPlaces: 0
        };

        $.extend(true, config, extraConfigs || {}); // merge extraConfigs into master config (defaults)

        var hcOptions = {
            chart: {
                type: 'gauge',
                spacingTop: -10,
                spacingBottom: -100,
                events: {
                    load: function () {
                        this.yAxis[0].ticks[0].label.translate(0, 10);
                    }
                }
            },
            credits: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            plotOptions: {
                gauge: {
                    pivot: {
                        radius: 6
                    }
                }
            },
            pane: {
                startAngle: -90,
                endAngle: 90,
                background: null,
            },
            title: null,
            // the value axis
            yAxis: {
                labels: {
                    //step: 3,
                    rotation: "horizontal",
                    distance: -12,
                    y: 24,
                    style: {
                        fontSize: '21px',
                        color: '#222',
                        fontFamily: 'Arial'
                    }
                },
                min: -100,
                max: 100,
                minorTickWidth: 0,
                tickWidth: 0,
                tickInterval: 100,
                title: {
                    text: null
                },
                plotBands: [{
                    from: -100,
                    to: -50,
                    thickness: 24,
                    color: config.colorDetractor || '#a8000b'
                }, {
                    from: -50,
                    to: 50,
                    thickness: 24,
                    color: config.colorNeutral || '#ffbb19'
                }, {
                    from: 50,
                    to: 100,
                    thickness: 24,
                    color: config.colorPromoter || '#00847e'
                }]
            },
            series: [{
                name: 'Overall Satisfaction',
                data: [parseFloat(data[0].Value)],
                dataLabels: {
                    //y: 50,

                    formatter: function () {
                        var ret = '<div style="text-align: center; display: inline-grid"><span>' + this.y.toFixed(config.decimalPlaces) + '</span>';
                        if (config.showCount === true) {
                            ret += '<span style="font-size: 13px;">n=' + data[0].Count.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '</span>';
                        }
                        ret += '</div>';

                        return ret;
                    },
                    style: {
                        textAlign: 'center',
                        fontFamily: 'Arial',
                        fontSize: '50px',
                        color: '#333',
                        fontWeight: '400'
                    },
                    borderWidth: 0,
                    useHTML: true
                },
                dial: {
                    baseLength: '10%',
                    radius: '100%',
                    baseWidth: 6,
                    topWidth: 2,
                    rearLength: '0%'
                }
            }]
        };
        self.$el.find(self._uiBindings.content).highcharts(hcOptions);
    }
})();