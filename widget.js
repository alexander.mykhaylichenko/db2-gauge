var extraConfigs = {
    colorDetractor: '#a8000b',
    colorNeutral: '#ffbb19',
    colorPromoter: '#00847e',
    showCount: false,
    decimalPlaces: 0
};

if (!window.loadDB2Gauge && !window.gettingGaugeScript) {
    window.gettingGaugeScript = true;
    $.getScript('https://platform-services.custom.eu.mcx.cloud/DB2Extensibility/gauge.js', function () {
        delete window.gettingGaugeScript;
        loadDB2Gauge(self, data, extraConfigs);
    });
} else if (window.gettingGaugeScript) {
    var wait = setInterval(function() {
        if (window.loadDB2Gauge) {
            clearInterval(wait)
            loadDB2Gauge(self, data, extraConfigs);
        }
    }, 500);
} else {
    loadDB2Gauge(self, data, extraConfigs);
}
