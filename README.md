# DB2 Gauge

DB2 Gauge is an extensible widget for Dashboards 2 allowing a gauge to be rendered in the reporting area using metric data.

![Snapshot of a gauge](snapshot.jpg?raw=true "Snapshot, a gauge in a dashboard with the count shown and rounded to one decimal place")

> **Hint: Dashboard PDF exports and snapshots ARE supported!**

## Usage

For now, each gauge has to be created manually using the extensibility widget provided by Dashboards. You must be a super admin to create these. See the steps below to set up a single gauge on a dashboard.

### Setup

First, you will need to configure the settings for the gauge. Below is an example configuration that you will need to modify to meet your requirements.

```Javascript
{
    colorDetractor: '#a8000b',
    colorNeutral: '#ffbb19',
    colorPromoter: '#00847e',
    showCount: false,
    decimalPlaces: 0
}
```

**These are all the currently supported configurations. See the example above as a reference**
- **colorDetractor**: must be a legal CSS colour value (see https://www.w3schools.com/cssref/css_colors_legal.asp). Optional, default is "#a8000b". The colour of the detractor section of the gauge.
- **colorNeutral**: must be a legal CSS colour value (see https://www.w3schools.com/cssref/css_colors_legal.asp). Optional, default is "#ffbb19". The colour of the neutral section of the gauge.
- **colorPromoter**: must be a legal CSS colour value (see https://www.w3schools.com/cssref/css_colors_legal.asp). Optional, default is "#00847e". The colour of the promoter portion of the gauge.
- **showCount**: must be a boolean. Optional, default is false. If true the widget will show the total count under the gauge in the same style as the standard Big Number chart.
- **decimalPlaces**: must be a positive integer. Optional, default is 0. The number of decimal places for the gauge value / big number.

> Hint: see https://www.w3schools.com/cssref/css_colors_legal.asp for all the legal CSS colour values.

> Hint: Multiple gauges are allowed per dashboard.

After the configuration options have been decided, it's time to prepare them for use, **in "widget.js" is all the code you will need to paste into the "Javascript" section of the widget editor**, open this file and add your required configuration into the "extraConfigs" variable at the top, the example configurations are already in there for reference, when this is done you can proceed to the Installation section.

**Your final JS section should look something like this:**

```Javascript
var extraConfigs = {
    colorDetractor: '#a8000b',
    colorNeutral: '#ffbb19',
    colorPromoter: '#00847e',
    showCount: false,
    decimalPlaces: 0
};

if (!window.loadDB2Gauge && !window.gettingGaugeScript) {
    window.gettingGaugeScript = true;
    $.getScript('https://platform-services.custom.eu.mcx.cloud/DB2Extensibility/gauge.js', function () {
        delete window.gettingGaugeScript;
        loadDB2Gauge(self, data, extraConfigs);
    });
} else if (window.gettingGaugeScript) {
    var wait = setInterval(function() {
        if (window.loadDB2Gauge) {
            clearInterval(wait)
            loadDB2Gauge(self, data, extraConfigs);
        }
    }, 500);
} else {
    loadDB2Gauge(self, data, extraConfigs);
}
```

### Installation

This assumes you have already prepared your required configurations in the Setup section above.

1. Sign in as the owner of the dashboard you would like to add the gauge to.
2. Enable the extensibility widget function by adding **?ext=1** into the URL after the domain, for example "...mcxplatform.de/?ext=1/#r/9e9bf3e9...", and hit enter.
3. Now you may drag in an extensibiltiy widget from the sidebar **Widgets > Extensible Content** to the desired position in your report
4. Open the widget's editor.
5. **First**, you need to add the data source, drag in your required data field into the metrics section from the correct source (survey) in the side panel.
6. In the **Text / Image** textbox add a single space.
7. **Lastly**, open the **Javascript** tab and paste in the prepared code with the configurations from the Setup section into the textbox.
8. Click Apply. _If no gauge shows see the Troubleshooting section_.
10. Enjoy your new gauge!

## Troubleshooting

### I have just created the gauge without any issues but the spinner just keeps spinning and nothing shows

Try reloading the page, if this still happens or it doesn't look right edit the widget and check all the changes you made in the installation steps still persist.

### I've just created the gauge but as soon as I clicked apply and return to the dashboard report it doesn't show the gauge

Sometimes when adding Javascript to an extebsibility widget it does not get saved, this seems to happen mostly when changing all the settings in the widget editor too quickly. Try going through steps 4 - 8 in the Installation section again, or check all of the things you did in those steps got saved when doing it the first time, if some things are missing just add them in again but with a couple of seconds or so between each step, and a wait a couple of seconds before you hit apply, also try to resist changing the name of the widget or making any other change during the installation process, you can change it after the gauge shows correctly.
